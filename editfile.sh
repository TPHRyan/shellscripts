#!/bin/bash

if [ "$#" -eq 0 ]
then
	$EDITOR
	exit 0
fi

eval "filearg=\$$#"

if [ -e "$filearg" -a ! -w "$filearg" ]
then
	echo "File exists and not writable, editing as root!"
	sudoedit "$@"
else
	echo "Editing as user!"
	$EDITOR "$@"
fi
exit 0
