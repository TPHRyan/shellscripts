#!/bin/bash

NETWORK_INTERFACE="wlp8s0"
NETWORK_SERVICE="netctl-auto@${NETWORK_INTERFACE}.service"

case $1 in
"--check")
    systemctl status "${NETWORK_SERVICE}"
    ;;
"--restart")
    echo "Restarting network service..."
    systemctl restart "${NETWORK_SERVICE}"
    echo "Network service restarted."
    ;;
*)
    echo "Usage: `basename $0` [--check] [--restart]"
    exit 2
    ;;
esac
