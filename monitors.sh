#!/bin/bash
function usage_message {
    echo "Usage: `basename $0` [--on] [--off]"
}

if [[ $# != 1 ]]
then
    usage_message
    exit 2
fi

xrandr --setprovideroutputsource 1 0
xrandr --setprovideroutputsource 2 0

case $1 in
"--on")
    xrandr --output DVI-I-2 --mode 1920x1080 --right-of LVDS1
    xrandr --output DVI-I-1 --mode 1920x1080 --right-of DVI-I-2
    ;;
"--off")
    xrandr --output DVI-I-2 --off
    xrandr --output DVI-I-1 --off
    ;;
*)
    usage_message
    ;;
esac
