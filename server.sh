#!/bin/bash

IDENTITY_FILE="/home/ryan/.ssh/aws-serveus.pem"
SERVER_URL="ubuntu@dev.serveuscentre.com.au"
BACKUP_LOCATION="/var/backups/serveus"
BACKUP_FILE="serveus_ambush_Mon.7z"
SQL_FILE="serveus_ambush.sql"
DB_NAME="serveus_ambush"

new_serveus_db() {
    scp -i "$IDENTITY_FILE" "$SERVER_URL":"$BACKUP_LOCATION"/"$BACKUP_FILE" ./ &&\
        7z e "$BACKUP_FILE" &&\
        mysql -u root -p "$DB_NAME" < "$SQL_FILE" &&\
        rm ./"$BACKUP_FILE" &&\
        rm ./"$SQL_FILE" ||\
        exit 2
}

case $1 in
"--start")
    echo "Starting development server..."
    sudo systemctl start httpd.service
    sudo systemctl start mysqld.service
    echo "Development server started."
    ;;
"--stop")
    echo "Stopping development server..."
    sudo systemctl stop httpd.service
    sudo systemctl stop mysqld.service
    echo "Development server stopped."
    ;;
"--respawn-serveus-db")
    echo "Obtaining new ServeUs DB..."
    new_serveus_db
    echo "ServeUs DB obtained."
    ;;
*)
    echo "Usage: `basename $0` [--start] [--stop]"
    exit 2
    ;;
esac
